#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPlainTextEdit>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

	QVector<QPlainTextEdit*> * editors;
	int newTabCount;

public slots:
	void newTab();
	void openTab();
	void saveAsTab();
	void saveTab();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
