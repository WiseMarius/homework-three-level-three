#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <vector>
#include <fstream>

#include <QTextStream>
#include <QPlainTextEdit>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	//init widgets vector
	editors = new QVector<QPlainTextEdit*>();
	//initialize the new-tab count
	newTabCount = 0;

	//add a new tab by default
	newTab();

	//connect the New action to a slot
	connect(ui->actionNew, SIGNAL(triggered(bool)),
		this, SLOT(newTab()));

	//connect the Open action to a slot
	connect(ui->actionOpen, SIGNAL(triggered(bool)),
		this, SLOT(openTab()));

	//connect the Open action to a slot
	connect(ui->actionSave_As, SIGNAL(triggered(bool)),
		this, SLOT(saveAsTab()));

	//connect the Open action to a slot
	connect(ui->actionSave, SIGNAL(triggered(bool)),
		this, SLOT(saveTab()));
}

void MainWindow::newTab() 
{
	//create a new widget
	QWidget * widget = new QWidget();

	char fileName[8] = "New ";
	char fileIndex[4];
	itoa(newTabCount++, fileIndex, 10);
		
	strcat_s(fileName, fileIndex);

	//add it to the tabWidget
	ui->tabWidget->addTab(widget, fileName);

	//add a plain text editor in the widget
	QPlainTextEdit * textEditor = new QPlainTextEdit(widget);
	//create a layout for the tab and add the text editor to the layout
	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(textEditor);
	widget->setLayout(layout);

	//add the new editor to the editors vector
	editors->push_back(textEditor);
}

void MainWindow::openTab()
{
	//open file dialog
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
		"",
		tr("Text Files (*.txt)"));
	
	//create a new widget
	QWidget * widget = new QWidget();
	
	//split the fileName path for extracting only the file name
	QStringList fileNameSplit = fileName.split("/");
	//add it to the tabWidget
	ui->tabWidget->addTab(widget, fileNameSplit[fileNameSplit.size() - 1]);

	//add the full path in the widget toolTip
	widget->setToolTip(fileName);

	//add a plain text editor in the widget
	QPlainTextEdit * textEditor = new QPlainTextEdit(widget);
	//create a layout for the tab and add the text editor to the layout
	QVBoxLayout *layout = new QVBoxLayout;
	layout->addWidget(textEditor);
	widget->setLayout(layout);

	//add the new editor to the editors vector
	editors->push_back(textEditor);
	
	//open the file
	QFile inputFile(fileName);
	if (inputFile.open(QIODevice::ReadOnly))
	{
		QTextStream inStream(&inputFile);
		while (!inStream.atEnd())
		{
			QString line = inStream.readLine();
			textEditor->appendPlainText(line);
		}
		inputFile.close();
	}
}

void MainWindow::saveAsTab()
{
	int currentTab = ui->tabWidget->currentIndex();

	//save file dialog
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save File As"),
		ui->tabWidget->tabText(currentTab),
		tr("Text Files (*.txt)"));

	//check if the file name length is greater than zero
	if (fileName.size() > 0) 
	{
		//write a new file
		std::ofstream out(fileName.toStdString());
		out << editors->at(currentTab)->toPlainText().toStdString();

		//split the fileName path for extracting only the file name
		QStringList fileNameSplit = fileName.split("/");
		//update the tab tooltip text
		ui->tabWidget->widget(currentTab)->setToolTip(fileName);
		//update the tab name
		ui->tabWidget->setTabText(currentTab, fileNameSplit[fileNameSplit.size() - 1]);
	}
}

void MainWindow::saveTab()
{
	int currentTab = ui->tabWidget->currentIndex();

	if (ui->tabWidget->widget(currentTab)->toolTip() == NULL) {
		saveAsTab();
	}
	else
	{
		//re-write the file
		std::ofstream out(ui->tabWidget->widget(currentTab)->toolTip().toStdString());
		out << editors->at(currentTab)->toPlainText().toStdString();
	}
}

MainWindow::~MainWindow()
{
    delete ui;
}
